type Symbol = Atom.atom
type Token  = Atom.atom

type Symbols = AtomSet.set
type Tokens  = AtomSet.set

type RHS = Atom.atom list

structure RHS_KEY : ORD_KEY =
struct
    type ord_key = RHS
    val compare = List.collate Atom.compare
end

structure RHSSet = RedBlackSetFn (RHS_KEY)

type Productions = RHSSet.set

type Rules = Productions AtomMap.map

type Grammar = {symbols : Symbols, tokens : Tokens, rules : Rules}

fun getnullableset nullable symbols symbol []        = (AtomSet.add(nullable,symbol))
  | getnullableset nullable symbols symbol (x :: xs) = if (AtomSet.member(symbols,x) andalso AtomSet.member(nullable, x)) then
                                                            getnullableset nullable symbols symbol xs
                                                       else
                                                            nullable

fun getfirstset first nullable symbols symbol []        = first
  | getfirstset first nullable symbols symbol (x :: xs) = let
                                                            val firstsym               = AtomMap.find(first,symbol)
                                                            val (firstd,symbolset)     = (case firstsym of
                                                                                            NONE     => (AtomMap.insert(first,symbol,AtomSet.empty),AtomSet.empty)
                                                                                            | SOME x => (first,x))

                                                            val updatedfirst = if (AtomSet.member(symbols,x)) then
                                                                                    let
                                                                                        val firstx              = AtomMap.find(firstd,x)
                                                                                        val (firstdd,xset) = (case firstx of
                                                                                                                  NONE   => (AtomMap.insert(firstd,x,AtomSet.empty),AtomSet.empty)
                                                                                                                | SOME x => (firstd,x))

                                                                                    in
                                                                                        AtomMap.insert(firstdd,symbol,AtomSet.union(xset,symbolset))
                                                                                    end
                                                                                else
                                                                                    AtomMap.insert(firstd,symbol,AtomSet.add(symbolset,x))
                                                        in
                                                            if(AtomSet.member(symbols,x) andalso AtomSet.member(nullable,x)) then
                                                                getfirstset updatedfirst nullable symbols symbol xs
                                                            else
                                                                updatedfirst
                                                        end

fun getfollowset follow nullable first firstset symbols symbol take []        = follow
  | getfollowset follow nullable first firstset symbols symbol take (x :: xs) = let
                                                                                    val followsym           = AtomMap.find(follow,symbol)
                                                                                    val (followd,symbolset) = (case followsym of
                                                                                                                NONE   => (AtomMap.insert(follow,symbol,AtomSet.empty),AtomSet.empty)
                                                                                                              | SOME x => (follow,x))

                                                                                    val (updatedfollow,updatedfirstset,updatedtake) = if (AtomSet.member(symbols,x)) then
                                                                                                                                        if(take) then
                                                                                                                                            let
                                                                                                                                                val followx = AtomMap.find(follow,x)
                                                                                                                                                val (followdd,xset) = (case followx of
                                                                                                                                                                        NONE   => (AtomMap.insert(follow,x,AtomSet.empty),AtomSet.empty)
                                                                                                                                                                      | SOME x => (follow,x))
                                                                                                                                                val firstx = AtomMap.find(first,x)
                                                                                                                                                val fix    = (case firstx of
                                                                                                                                                                NONE   => AtomSet.empty
                                                                                                                                                              | SOME x => x)
                                                                                                                                                val (taked,firstsetd) = if(AtomSet.member(nullable,x)) then
                                                                                                                                                                            (take,AtomSet.union(firstset,fix))
                                                                                                                                                                        else
                                                                                                                                                                            (false,fix)
                                                                                                                                            in
                                                                                                                                                (AtomMap.insert(follow,x,AtomSet.union(AtomSet.union(symbolset,xset),firstset)),firstsetd,taked)
                                                                                                                                            end
                                                                                                                                        else
                                                                                                                                            let
                                                                                                                                                val followx = AtomMap.find(follow,x)
                                                                                                                                                val (followdd,xset) = (case followx of
                                                                                                                                                                        NONE   => (AtomMap.insert(follow,x,AtomSet.empty),AtomSet.empty)
                                                                                                                                                                      | SOME x => (follow,x))
                                                                                                                                                val firstx = AtomMap.find(first,x)
                                                                                                                                                val fix    = (case firstx of
                                                                                                                                                                NONE   => AtomSet.empty
                                                                                                                                                              | SOME x => x)
                                                                                                                                                val (taked,firstsetd) = if(AtomSet.member(nullable,x)) then
                                                                                                                                                                            (take,AtomSet.union(firstset,fix))
                                                                                                                                                                        else
                                                                                                                                                                            (false,fix)
                                                                                                                                            in
                                                                                                                                                (AtomMap.insert(follow,x,AtomSet.union(xset,firstset)),firstsetd,taked)
                                                                                                                                            end
                                                                                                                                      else
                                                                                                                                        (follow,AtomSet.singleton(x),false)
                                                                                in
                                                                                    getfollowset updatedfollow nullable first updatedfirstset symbols symbol updatedtake xs
                                                                                end


fun forallprod symbol nullable first follow symbols []        = (nullable, first, follow)
  | forallprod symbol nullable first follow symbols (x :: xs) = let
                                                                    val updatednullable = getnullableset nullable symbols symbol x
                                                                    val updatedfirst    = getfirstset first updatednullable symbols symbol x
                                                                    val updatedfollow   = getfollowset follow updatednullable updatedfirst (AtomSet.empty) symbols symbol true (List.rev x)
                                                                in
                                                                    forallprod symbol updatednullable updatedfirst updatedfollow symbols xs
                                                                end


fun foreachprod gmap nullable first follow symbols []        = (nullable, first,follow)
  | foreachprod gmap nullable first follow symbols (x :: xs) = let
                                                                val xlist  = AtomMap.find(gmap,x)
                                                                val xxlist = (case xlist of
                                                                                NONE   => []
                                                                              | SOME x => RHSSet.listItems(x))
                                                                val (updatednullable,updatedfirst,updatedfollow) = forallprod x nullable first follow symbols xxlist
                                                               in
                                                                foreachprod gmap updatednullable updatedfirst updatedfollow symbols xs
                                                               end

fun foreachprodd nullable first follow symbols gmap = foreachprod gmap nullable first follow symbols (AtomMap.listKeys gmap) 

fun repeatuntilnochange nullable first follow symbols gmap sizenullable sizefirst sizefollow = let
                                                                                                val (updatednullable, updatedfirst, updatedfollow) = foreachprodd nullable first follow symbols gmap
                                                                                                val updatednullablesize = AtomSet.numItems updatednullable
                                                                                                val updatedfirstsize    = AtomMap.numItems updatedfirst
                                                                                                val updatedfollowsize   = AtomMap.numItems updatedfollow
                                                                                               in
                                                                                                if(updatednullablesize=sizenullable andalso updatedfirstsize=sizefirst andalso updatedfollowsize=sizefollow) then 
                                                                                                    (updatednullable,updatedfirst,updatedfollow)
                                                                                                else
                                                                                                    repeatuntilnochange updatednullable updatedfirst updatedfollow symbols gmap updatednullablesize updatedfirstsize updatedfollowsize
                                                                                                end

fun printlist []      = ""
  | printlist (x::xs) = (Atom.toString x) ^ " " ^ (printlist xs)


(* Grammar to check the above algorithm *)

val atomX      = Atom.atom("X")
val atomY      = Atom.atom("Y")
val atomZ      = Atom.atom("Z")
val symbollist = [Atom.atom("Z"), Atom.atom("Y"), Atom.atom("X")]
val symbols    = AtomSet.fromList symbollist
val nullable   = AtomSet.empty
val first      = AtomMap.empty
val follow     = AtomMap.empty
val Z1         = [Atom.atom("d")]
val Z2         = [Atom.atom("X"), Atom.atom("Y"), Atom.atom("Z")]
val Z3         = []
val Z          = [Z1, Z2, Z3]
val rhsZ       = RHSSet.fromList Z
val Y1         = []
val Y2         = [Atom.atom("c")]
val Y3         = [atomX, atomY]
val Y          = [Y1,Y2,Y3]
val rhsY       = RHSSet.fromList Y
val X1         = [atomZ,atomY]
val X2         = [Atom.atom("a"),Atom.atom("c")]
val X          = [X1,X2]
val rhsX       = RHSSet.fromList X
val gmap       = AtomMap.singleton(Atom.atom("Z"),rhsZ)
val gmapp      = AtomMap.insert(gmap,Atom.atom("Y"),rhsY)
val gmappp     = AtomMap.insert(gmapp,Atom.atom("X"),rhsX)

val (nullable,first,follow) = repeatuntilnochange nullable first follow symbols gmappp 0 0 0

val nullables = printlist (AtomSet.listItems nullable)
val firstX    = printlist (AtomSet.listItems (AtomMap.lookup(first,atomX)))
val firstY    = printlist (AtomSet.listItems (AtomMap.lookup(first,atomY)))
val firstZ    = printlist (AtomSet.listItems (AtomMap.lookup(first,atomZ)))
val followX   = printlist (AtomSet.listItems (AtomMap.lookup(follow,atomX)))
val followY   = printlist (AtomSet.listItems (AtomMap.lookup(follow,atomY)))
val followZ   = printlist (AtomSet.listItems (AtomMap.lookup(follow,atomZ)))