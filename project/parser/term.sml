structure Color =
struct
  
    fun keywords_color () = "\027[31m"
    fun symbols_color ()  = "\027[38;5;3m"
    fun rest_color ()     = "\027[37m"
    fun digits_color ()   = "\027[34m"
    fun comments_color () = "\027[38;5;239m"
    fun quoted_color ()   = "\027[38;5;216m"

end