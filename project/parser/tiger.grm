%%

%eop EOF
%verbose
%pure

%name Tiger

%term EOF
    | NIL
    | ID of string
    | INT of string
    | STRING of string
    | PLUS | MINUS | DIV | MUL | GT | GE | LT | LE | EQ | AND | OR | NE
    | ASSIGN | ARRAY | IF | THEN | ELSE | WHILE | FOR | TO | DO | LBRACK | RBRACK
    | LPAREN | RPAREN | LBRACE | RBRACE | COMMA | SEMICOLON | COLON | OF | DOT
    | TYPE | VAR | LET | IN | END | FUNCTION | BREAK

%nonterm program of Ast.program
       | exp of Ast.exp
       | exps of Ast.exp list
       | decs of Ast.dec list
       | dec of Ast.dec
       | ty of Ast.typ
       | tyfields of (string*string) list
       | funparams of Ast.exp list
       | lvalue of Ast.var


%pos int
%start program

%noshift EOF
%nonassoc ASSIGN
%nonassoc ID
%nonassoc WHILE DO
%right THEN
%right ELSE
%nonassoc LBRACK RBRACK OF
%left OR
%left AND
%nonassoc EQ NE LT LE GT GE
%left PLUS MINUS
%left MUL DIV

%prefer THEN ELSE LPAREN

%%

program : exp                         (Ast.Expression exp)
        | decs                        (Ast.Declaration decs)

exp : NIL                             (Ast.NilExp)
    | INT                             (Ast.IntExp INT) 
    | ID                              (Ast.ID ID)
    | exp PLUS exp                    (Ast.OpExp (exp1,Ast.PLUS,exp2))
    | exp MINUS exp                   (Ast.OpExp (exp1,Ast.MINUS,exp2))
    | exp MUL exp                     (Ast.OpExp (exp1,Ast.MUL,exp2))
    | exp DIV exp                     (Ast.OpExp (exp1,Ast.PLUS,exp2))
    | exp GT exp                      (Ast.OpExp (exp1,Ast.GT,exp2))
    | exp GE exp                      (Ast.OpExp (exp1,Ast.GE,exp2))
    | exp LT exp                      (Ast.OpExp (exp1,Ast.LT,exp2))
    | exp LE exp                      (Ast.OpExp (exp1,Ast.LE,exp2))
    | exp EQ exp                      (Ast.OpExp (exp1,Ast.EQ,exp2))
    | exp AND exp                     (Ast.OpExp (exp1,Ast.AND,exp2))
    | exp OR exp                      (Ast.OpExp (exp1,Ast.OR,exp2))
    | exp NE exp                      (Ast.OpExp (exp1,Ast.NE,exp2))
    | MINUS exp                       (Ast.NegExp (exp))
    | ID ASSIGN exp                   (Ast.AssignExp (ID,exp))
    | IF exp THEN exp ELSE exp        (Ast.IfExp (exp1,exp2,exp3))
    | IF exp THEN exp                 (Ast.IfThenExp (exp1,exp2))
    | WHILE exp DO exp                (Ast.WhileExp (exp1,exp2)) 
    | FOR ID ASSIGN exp TO exp DO exp (Ast.ForExp (ID,exp1,exp2,exp3))
    | ID LBRACK exp RBRACK OF exp   (Ast.ArrayExp (ID,exp1, exp2))
    | BREAK                           (Ast.BreakExp)      
    | LET decs IN exps END            (Ast.LetExp (decs,exps))
    | ID LPAREN funparams RPAREN      (Ast.FunCall (ID, funparams))
    | LPAREN exp RPAREN               (exp)
    | lvalue                          (Ast.LValue lvalue)

lvalue : ID LBRACK exp RBRACK         (Ast.SubscriptVar(ID,exp))

funparams :                           ([])
          | exp                       ([exp])
          | exp COMMA funparams       (exp :: funparams)


exps  : exp                           ([exp])
      | exp SEMICOLON exps            (exp :: exps)

decs : dec                            ([dec])
     | dec decs                       (dec :: decs)

dec : TYPE ID EQ ty                   (Ast.TypeDec (ID,ty))
    | VAR ID ASSIGN exp                       (Ast.VarDec (ID,exp))
    | VAR ID COLON ID ASSIGN exp                       (Ast.VartypeDec (ID1,ID2,exp))
    | FUNCTION ID LPAREN tyfields RPAREN EQ exp (Ast.FuncDec (ID,tyfields,exp))
    | FUNCTION ID LPAREN tyfields RPAREN COLON ID EQ exp (Ast.FunctypeDec (ID1,tyfields,ID2,exp))


ty : LBRACE tyfields RBRACE           (Ast.record (tyfields))
   | ARRAY OF ID                      (Ast.array (ID))

tyfields :                              ([]) 
         | ID COLON ID                ([(ID1,ID2)])
         | ID COLON ID COMMA tyfields    ((ID1,ID2) :: tyfields)
