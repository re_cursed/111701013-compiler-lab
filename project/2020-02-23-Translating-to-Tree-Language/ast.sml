structure Ast =
struct

    datatype binop = PLUS | MINUS | MUL | DIV | GT | GE | LT | LE | EQ | AND | OR | NE

    datatype typ = alias of string
                  | record of (string*string) list
                  | array of string

    datatype var  = SimpleVar of string
                  | FieldVar of var*string
                  | SubscriptVar of string*exp
  
    and exp      = NilExp
                 | IntExp of string
                 | ID of string
                 | OpExp of exp*binop*exp
                 | NegExp of exp
                 | AssignExp of string*exp
                 | IfExp of exp*exp*exp
                 | IfThenExp of exp*exp
                 | WhileExp of exp*exp
                 | ForExp of string*exp*exp*exp
                 | ArrayExp of string*exp*exp
                 | FunCall of string*exp list
                 | BreakExp
                 | LValue of var
                 | LetExp of dec list*exp list
  
    and dec      = TypeDec of string*typ
                 | VarDec of string*exp
                 | VartypeDec of string*string*exp
                 | FuncDec of string*(string*string) list*exp
                 | FunctypeDec of string*(string*string) list*string*exp


    datatype program = Expression of exp
                     | Declaration of dec list

end