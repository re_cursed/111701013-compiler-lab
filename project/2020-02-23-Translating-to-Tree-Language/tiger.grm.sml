functor TigerLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Tiger_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
(*#line 1.2 "tiger.grm"*)
(*#line 12.1 "tiger.grm.sml"*)
end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\000\000\000\000\
\\001\000\001\000\124\000\006\000\032\000\007\000\031\000\008\000\030\000\
\\009\000\029\000\015\000\124\000\016\000\124\000\021\000\124\000\
\\022\000\124\000\025\000\124\000\026\000\124\000\028\000\124\000\
\\030\000\124\000\033\000\124\000\034\000\124\000\038\000\124\000\
\\039\000\124\000\041\000\124\000\042\000\124\000\043\000\124\000\000\000\
\\001\000\001\000\125\000\006\000\032\000\007\000\031\000\008\000\030\000\
\\009\000\029\000\015\000\125\000\016\000\125\000\021\000\125\000\
\\022\000\125\000\025\000\125\000\026\000\125\000\028\000\125\000\
\\030\000\125\000\033\000\125\000\034\000\125\000\038\000\125\000\
\\039\000\125\000\041\000\125\000\042\000\125\000\043\000\125\000\000\000\
\\001\000\001\000\126\000\006\000\032\000\007\000\031\000\008\000\030\000\
\\009\000\029\000\015\000\126\000\016\000\126\000\021\000\126\000\
\\022\000\126\000\025\000\126\000\026\000\126\000\028\000\126\000\
\\030\000\126\000\033\000\126\000\034\000\126\000\038\000\126\000\
\\039\000\126\000\041\000\126\000\042\000\126\000\043\000\126\000\000\000\
\\001\000\001\000\127\000\006\000\032\000\007\000\031\000\008\000\030\000\
\\009\000\029\000\015\000\127\000\016\000\127\000\021\000\127\000\
\\022\000\127\000\025\000\127\000\026\000\127\000\028\000\127\000\
\\030\000\127\000\033\000\127\000\034\000\127\000\038\000\127\000\
\\039\000\127\000\041\000\127\000\042\000\127\000\043\000\127\000\000\000\
\\001\000\001\000\128\000\006\000\032\000\007\000\031\000\008\000\030\000\
\\009\000\029\000\015\000\128\000\016\000\128\000\021\000\128\000\
\\022\000\128\000\025\000\128\000\026\000\128\000\028\000\128\000\
\\030\000\128\000\033\000\128\000\034\000\128\000\038\000\128\000\
\\039\000\128\000\041\000\128\000\042\000\128\000\043\000\128\000\000\000\
\\001\000\001\000\131\000\006\000\032\000\007\000\031\000\008\000\030\000\
\\009\000\029\000\015\000\131\000\016\000\131\000\021\000\131\000\
\\022\000\131\000\025\000\131\000\026\000\131\000\028\000\131\000\
\\030\000\131\000\033\000\131\000\034\000\131\000\038\000\131\000\
\\039\000\131\000\041\000\131\000\042\000\131\000\043\000\131\000\000\000\
\\001\000\002\000\019\000\003\000\018\000\004\000\017\000\007\000\016\000\
\\020\000\015\000\023\000\014\000\024\000\013\000\029\000\012\000\
\\038\000\011\000\039\000\010\000\040\000\009\000\043\000\008\000\
\\044\000\007\000\000\000\
\\001\000\002\000\019\000\003\000\018\000\004\000\017\000\007\000\016\000\
\\020\000\015\000\023\000\014\000\024\000\013\000\029\000\012\000\
\\040\000\009\000\044\000\007\000\000\000\
\\001\000\003\000\033\000\000\000\
\\001\000\003\000\035\000\000\000\
\\001\000\003\000\036\000\000\000\
\\001\000\003\000\038\000\000\000\
\\001\000\003\000\074\000\000\000\
\\001\000\003\000\098\000\000\000\
\\001\000\003\000\102\000\000\000\
\\001\000\003\000\106\000\000\000\
\\001\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\
\\021\000\065\000\000\000\
\\001\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\
\\025\000\092\000\000\000\
\\001\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\
\\026\000\064\000\000\000\
\\001\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\
\\026\000\109\000\000\000\
\\001\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\
\\028\000\084\000\000\000\
\\001\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\
\\030\000\062\000\000\000\
\\001\000\014\000\061\000\000\000\
\\001\000\014\000\097\000\035\000\096\000\000\000\
\\001\000\014\000\110\000\000\000\
\\001\000\018\000\060\000\035\000\059\000\000\000\
\\001\000\018\000\063\000\000\000\
\\001\000\018\000\089\000\000\000\
\\001\000\019\000\078\000\031\000\077\000\000\000\
\\001\000\029\000\057\000\000\000\
\\001\000\030\000\082\000\000\000\
\\001\000\030\000\085\000\000\000\
\\001\000\032\000\101\000\000\000\
\\001\000\035\000\086\000\000\000\
\\001\000\036\000\091\000\000\000\
\\001\000\038\000\011\000\039\000\010\000\043\000\008\000\000\000\
\\001\000\041\000\058\000\000\000\
\\001\000\042\000\087\000\000\000\
\\115\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\116\000\000\000\
\\117\000\000\000\
\\118\000\000\000\
\\119\000\018\000\044\000\027\000\043\000\029\000\042\000\000\000\
\\120\000\008\000\030\000\009\000\029\000\000\000\
\\121\000\008\000\030\000\009\000\029\000\000\000\
\\122\000\000\000\
\\123\000\000\000\
\\129\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\017\000\021\000\000\000\
\\130\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\017\000\021\000\000\000\
\\132\000\008\000\030\000\009\000\029\000\000\000\
\\133\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\134\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\135\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\
\\022\000\093\000\000\000\
\\136\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\137\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\138\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\139\000\000\000\
\\140\000\000\000\
\\141\000\000\000\
\\142\000\000\000\
\\143\000\000\000\
\\144\000\036\000\095\000\000\000\
\\145\000\002\000\019\000\003\000\018\000\004\000\017\000\007\000\016\000\
\\020\000\015\000\023\000\014\000\024\000\013\000\029\000\012\000\
\\040\000\009\000\044\000\007\000\000\000\
\\146\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\
\\033\000\083\000\000\000\
\\147\000\000\000\
\\148\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\
\\034\000\088\000\000\000\
\\149\000\000\000\
\\150\000\038\000\011\000\039\000\010\000\043\000\008\000\000\000\
\\151\000\000\000\
\\152\000\000\000\
\\153\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\154\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\155\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\156\000\006\000\032\000\007\000\031\000\008\000\030\000\009\000\029\000\
\\010\000\028\000\011\000\027\000\012\000\026\000\013\000\025\000\
\\014\000\024\000\015\000\023\000\016\000\022\000\017\000\021\000\000\000\
\\157\000\000\000\
\\158\000\000\000\
\\159\000\003\000\071\000\000\000\
\\160\000\033\000\108\000\000\000\
\\161\000\000\000\
\"
val actionRowNumbers =
"\007\000\061\000\068\000\040\000\
\\039\000\057\000\009\000\036\000\
\\010\000\011\000\008\000\012\000\
\\008\000\008\000\008\000\042\000\
\\043\000\041\000\069\000\008\000\
\\008\000\008\000\008\000\008\000\
\\008\000\008\000\008\000\008\000\
\\008\000\008\000\008\000\030\000\
\\037\000\026\000\023\000\022\000\
\\027\000\019\000\017\000\050\000\
\\063\000\008\000\008\000\006\000\
\\049\000\048\000\005\000\004\000\
\\003\000\002\000\001\000\046\000\
\\047\000\045\000\044\000\077\000\
\\008\000\013\000\008\000\029\000\
\\060\000\008\000\008\000\008\000\
\\031\000\064\000\021\000\051\000\
\\032\000\034\000\038\000\066\000\
\\028\000\071\000\070\000\077\000\
\\035\000\018\000\054\000\053\000\
\\059\000\063\000\062\000\024\000\
\\014\000\058\000\008\000\008\000\
\\033\000\015\000\008\000\008\000\
\\065\000\008\000\016\000\008\000\
\\078\000\067\000\072\000\075\000\
\\076\000\020\000\052\000\056\000\
\\025\000\073\000\077\000\008\000\
\\008\000\079\000\055\000\074\000\
\\000\000"
val gotoT =
"\
\\001\000\112\000\002\000\004\000\004\000\003\000\005\000\002\000\
\\009\000\001\000\000\000\
\\000\000\
\\004\000\018\000\005\000\002\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\032\000\005\000\002\000\000\000\
\\000\000\
\\000\000\
\\002\000\035\000\009\000\001\000\000\000\
\\000\000\
\\002\000\037\000\009\000\001\000\000\000\
\\002\000\038\000\009\000\001\000\000\000\
\\002\000\039\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\043\000\009\000\001\000\000\000\
\\002\000\044\000\009\000\001\000\000\000\
\\002\000\045\000\009\000\001\000\000\000\
\\002\000\046\000\009\000\001\000\000\000\
\\002\000\047\000\009\000\001\000\000\000\
\\002\000\048\000\009\000\001\000\000\000\
\\002\000\049\000\009\000\001\000\000\000\
\\002\000\050\000\009\000\001\000\000\000\
\\002\000\051\000\009\000\001\000\000\000\
\\002\000\052\000\009\000\001\000\000\000\
\\002\000\053\000\009\000\001\000\000\000\
\\002\000\054\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\065\000\008\000\064\000\009\000\001\000\000\000\
\\002\000\066\000\009\000\001\000\000\000\
\\002\000\067\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\068\000\000\000\
\\002\000\071\000\003\000\070\000\009\000\001\000\000\000\
\\000\000\
\\002\000\073\000\009\000\001\000\000\000\
\\006\000\074\000\000\000\
\\000\000\
\\002\000\077\000\009\000\001\000\000\000\
\\002\000\078\000\009\000\001\000\000\000\
\\002\000\079\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\088\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\065\000\008\000\092\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\071\000\003\000\097\000\009\000\001\000\000\000\
\\002\000\098\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\002\000\101\000\009\000\001\000\000\000\
\\002\000\102\000\009\000\001\000\000\000\
\\000\000\
\\002\000\103\000\009\000\001\000\000\000\
\\000\000\
\\002\000\105\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\109\000\000\000\
\\002\000\110\000\009\000\001\000\000\000\
\\002\000\111\000\009\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 113
val numrules = 47
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit | STRING of  (string) | INT of  (string) | ID of  (string) | lvalue of  (Ast.var) | funparams of  (Ast.exp list) | tyfields of  ( ( string*string )  list) | ty of  (Ast.typ) | dec of  (Ast.dec) | decs of  (Ast.dec list) | exps of  (Ast.exp list) | exp of  (Ast.exp) | program of  (Ast.program)
end
type svalue = MlyValue.svalue
type result = Ast.program
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn _ => false
val preferred_change : (term list * term list) list = 
(nil
,nil
 $$ (T 20))::
(nil
,nil
 $$ (T 21))::
(nil
,nil
 $$ (T 28))::
nil
val noShift = 
fn (T 0) => true | _ => false
val showTerminal =
fn (T 0) => "EOF"
  | (T 1) => "NIL"
  | (T 2) => "ID"
  | (T 3) => "INT"
  | (T 4) => "STRING"
  | (T 5) => "PLUS"
  | (T 6) => "MINUS"
  | (T 7) => "DIV"
  | (T 8) => "MUL"
  | (T 9) => "GT"
  | (T 10) => "GE"
  | (T 11) => "LT"
  | (T 12) => "LE"
  | (T 13) => "EQ"
  | (T 14) => "AND"
  | (T 15) => "OR"
  | (T 16) => "NE"
  | (T 17) => "ASSIGN"
  | (T 18) => "ARRAY"
  | (T 19) => "IF"
  | (T 20) => "THEN"
  | (T 21) => "ELSE"
  | (T 22) => "WHILE"
  | (T 23) => "FOR"
  | (T 24) => "TO"
  | (T 25) => "DO"
  | (T 26) => "LBRACK"
  | (T 27) => "RBRACK"
  | (T 28) => "LPAREN"
  | (T 29) => "RPAREN"
  | (T 30) => "LBRACE"
  | (T 31) => "RBRACE"
  | (T 32) => "COMMA"
  | (T 33) => "SEMICOLON"
  | (T 34) => "COLON"
  | (T 35) => "OF"
  | (T 36) => "DOT"
  | (T 37) => "TYPE"
  | (T 38) => "VAR"
  | (T 39) => "LET"
  | (T 40) => "IN"
  | (T 41) => "END"
  | (T 42) => "FUNCTION"
  | (T 43) => "BREAK"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 43) $$ (T 42) $$ (T 41) $$ (T 40) $$ (T 39) $$ (T 38) $$ (T 37) $$ (T 36) $$ (T 35) $$ (T 34) $$ (T 33) $$ (T 32) $$ (T 31) $$ (T 30) $$ (T 29) $$ (T 28) $$ (T 27) $$ (T 26) $$ (T 25) $$ (T 24) $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20) $$ (T 19) $$ (T 18) $$ (T 17) $$ (T 16) $$ (T 15) $$ (T 14) $$ (T 13) $$ (T 12) $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ (T 5) $$ (T 1) $$ (T 0)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.exp exp, exp1left, exp1right)) :: rest671)) => let val  result = MlyValue.program ((*#line 50.40 "tiger.grm"*)Ast.Expression exp(*#line 477.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, exp1left, exp1right), rest671)
end
|  ( 1, ( ( _, ( MlyValue.decs decs, decs1left, decs1right)) :: rest671)) => let val  result = MlyValue.program ((*#line 51.40 "tiger.grm"*)Ast.Declaration decs(*#line 481.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, decs1left, decs1right), rest671)
end
|  ( 2, ( ( _, ( _, NIL1left, NIL1right)) :: rest671)) => let val  result = MlyValue.exp ((*#line 53.40 "tiger.grm"*)Ast.NilExp(*#line 485.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, NIL1left, NIL1right), rest671)
end
|  ( 3, ( ( _, ( MlyValue.INT INT, INT1left, INT1right)) :: rest671)) => let val  result = MlyValue.exp ((*#line 54.40 "tiger.grm"*)Ast.IntExp INT(*#line 489.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, INT1left, INT1right), rest671)
end
|  ( 4, ( ( _, ( MlyValue.ID ID, ID1left, ID1right)) :: rest671)) => let val  result = MlyValue.exp ((*#line 55.40 "tiger.grm"*)Ast.ID ID(*#line 493.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, ID1left, ID1right), rest671)
end
|  ( 5, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 56.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.PLUS,exp2)(*#line 497.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 6, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 57.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.MINUS,exp2)(*#line 501.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 7, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 58.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.MUL,exp2)(*#line 505.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 8, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 59.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.PLUS,exp2)(*#line 509.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 9, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 60.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.GT,exp2)(*#line 513.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 10, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 61.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.GE,exp2)(*#line 517.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 11, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 62.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.LT,exp2)(*#line 521.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 63.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.LE,exp2)(*#line 525.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 13, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 64.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.EQ,exp2)(*#line 529.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 14, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 65.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.AND,exp2)(*#line 533.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 15, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 66.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.OR,exp2)(*#line 537.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 16, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, exp1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 67.40 "tiger.grm"*)Ast.OpExp (exp1,Ast.NE,exp2)(*#line 541.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, exp1left, exp2right), rest671)
end
|  ( 17, ( ( _, ( MlyValue.exp exp, _, exp1right)) :: ( _, ( _, MINUS1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 68.40 "tiger.grm"*)Ast.NegExp (exp)(*#line 545.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, MINUS1left, exp1right), rest671)
end
|  ( 18, ( ( _, ( MlyValue.exp exp, _, exp1right)) :: _ :: ( _, ( MlyValue.ID ID, ID1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 69.40 "tiger.grm"*)Ast.AssignExp (ID,exp)(*#line 549.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, ID1left, exp1right), rest671)
end
|  ( 19, ( ( _, ( MlyValue.exp exp3, _, exp3right)) :: _ :: ( _, ( MlyValue.exp exp2, _, _)) :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: ( _, ( _, IF1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 70.40 "tiger.grm"*)Ast.IfExp (exp1,exp2,exp3)(*#line 553.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, IF1left, exp3right), rest671)
end
|  ( 20, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: ( _, ( _, IF1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 71.40 "tiger.grm"*)Ast.IfThenExp (exp1,exp2)(*#line 557.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, IF1left, exp2right), rest671)
end
|  ( 21, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: ( _, ( _, WHILE1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 72.40 "tiger.grm"*)Ast.WhileExp (exp1,exp2)(*#line 561.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, WHILE1left, exp2right), rest671)
end
|  ( 22, ( ( _, ( MlyValue.exp exp3, _, exp3right)) :: _ :: ( _, ( MlyValue.exp exp2, _, _)) :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: _ :: ( _, ( MlyValue.ID ID, _, _)) :: ( _, ( _, FOR1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 73.40 "tiger.grm"*)Ast.ForExp (ID,exp1,exp2,exp3)(*#line 565.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, FOR1left, exp3right), rest671)
end
|  ( 23, ( ( _, ( MlyValue.exp exp2, _, exp2right)) :: _ :: _ :: ( _, ( MlyValue.exp exp1, _, _)) :: _ :: ( _, ( MlyValue.ID ID, ID1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 74.38 "tiger.grm"*)Ast.ArrayExp (ID,exp1, exp2)(*#line 569.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, ID1left, exp2right), rest671)
end
|  ( 24, ( ( _, ( _, BREAK1left, BREAK1right)) :: rest671)) => let val  result = MlyValue.exp ((*#line 75.40 "tiger.grm"*)Ast.BreakExp(*#line 573.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, BREAK1left, BREAK1right), rest671)
end
|  ( 25, ( ( _, ( _, _, END1right)) :: ( _, ( MlyValue.exps exps, _, _)) :: _ :: ( _, ( MlyValue.decs decs, _, _)) :: ( _, ( _, LET1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 76.40 "tiger.grm"*)Ast.LetExp (decs,exps)(*#line 577.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, LET1left, END1right), rest671)
end
|  ( 26, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.funparams funparams, _, _)) :: _ :: ( _, ( MlyValue.ID ID, ID1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 77.40 "tiger.grm"*)Ast.FunCall (ID, funparams)(*#line 581.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, ID1left, RPAREN1right), rest671)
end
|  ( 27, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.exp exp, _, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let val  result = MlyValue.exp ((*#line 78.40 "tiger.grm"*)exp(*#line 585.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 28, ( ( _, ( MlyValue.lvalue lvalue, lvalue1left, lvalue1right)) :: rest671)) => let val  result = MlyValue.exp ((*#line 79.40 "tiger.grm"*)Ast.LValue lvalue(*#line 589.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, lvalue1left, lvalue1right), rest671)
end
|  ( 29, ( ( _, ( _, _, RBRACK1right)) :: ( _, ( MlyValue.exp exp, _, _)) :: _ :: ( _, ( MlyValue.ID ID, ID1left, _)) :: rest671)) => let val  result = MlyValue.lvalue ((*#line 81.40 "tiger.grm"*)Ast.SubscriptVar(ID,exp)(*#line 593.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 8, ( result, ID1left, RBRACK1right), rest671)
end
|  ( 30, ( rest671)) => let val  result = MlyValue.funparams ((*#line 83.40 "tiger.grm"*)[](*#line 597.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 7, ( result, defaultPos, defaultPos), rest671)
end
|  ( 31, ( ( _, ( MlyValue.exp exp, exp1left, exp1right)) :: rest671)) => let val  result = MlyValue.funparams ((*#line 84.40 "tiger.grm"*)[exp](*#line 601.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 7, ( result, exp1left, exp1right), rest671)
end
|  ( 32, ( ( _, ( MlyValue.funparams funparams, _, funparams1right)) :: _ :: ( _, ( MlyValue.exp exp, exp1left, _)) :: rest671)) => let val  result = MlyValue.funparams ((*#line 85.40 "tiger.grm"*)exp :: funparams(*#line 605.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 7, ( result, exp1left, funparams1right), rest671)
end
|  ( 33, ( ( _, ( MlyValue.exp exp, exp1left, exp1right)) :: rest671)) => let val  result = MlyValue.exps ((*#line 88.40 "tiger.grm"*)[exp](*#line 609.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 2, ( result, exp1left, exp1right), rest671)
end
|  ( 34, ( ( _, ( MlyValue.exps exps, _, exps1right)) :: _ :: ( _, ( MlyValue.exp exp, exp1left, _)) :: rest671)) => let val  result = MlyValue.exps ((*#line 89.40 "tiger.grm"*)exp :: exps(*#line 613.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 2, ( result, exp1left, exps1right), rest671)
end
|  ( 35, ( ( _, ( MlyValue.dec dec, dec1left, dec1right)) :: rest671)) => let val  result = MlyValue.decs ((*#line 91.40 "tiger.grm"*)[dec](*#line 617.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 3, ( result, dec1left, dec1right), rest671)
end
|  ( 36, ( ( _, ( MlyValue.decs decs, _, decs1right)) :: ( _, ( MlyValue.dec dec, dec1left, _)) :: rest671)) => let val  result = MlyValue.decs ((*#line 92.40 "tiger.grm"*)dec :: decs(*#line 621.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 3, ( result, dec1left, decs1right), rest671)
end
|  ( 37, ( ( _, ( MlyValue.ty ty, _, ty1right)) :: _ :: ( _, ( MlyValue.ID ID, _, _)) :: ( _, ( _, TYPE1left, _)) :: rest671)) => let val  result = MlyValue.dec ((*#line 94.40 "tiger.grm"*)Ast.TypeDec (ID,ty)(*#line 625.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 4, ( result, TYPE1left, ty1right), rest671)
end
|  ( 38, ( ( _, ( MlyValue.exp exp, _, exp1right)) :: _ :: ( _, ( MlyValue.ID ID, _, _)) :: ( _, ( _, VAR1left, _)) :: rest671)) => let val  result = MlyValue.dec ((*#line 95.48 "tiger.grm"*)Ast.VarDec (ID,exp)(*#line 629.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 4, ( result, VAR1left, exp1right), rest671)
end
|  ( 39, ( ( _, ( MlyValue.exp exp, _, exp1right)) :: _ :: ( _, ( MlyValue.ID ID2, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, VAR1left, _)) :: rest671)) => let val  result = MlyValue.dec ((*#line 96.57 "tiger.grm"*)Ast.VartypeDec (ID1,ID2,exp)(*#line 633.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 4, ( result, VAR1left, exp1right), rest671)
end
|  ( 40, ( ( _, ( MlyValue.exp exp, _, exp1right)) :: _ :: _ :: ( _, ( MlyValue.tyfields tyfields, _, _)) :: _ :: ( _, ( MlyValue.ID ID, _, _)) :: ( _, ( _, FUNCTION1left, _)) :: rest671)) => let val  result = MlyValue.dec ((*#line 97.50 "tiger.grm"*)Ast.FuncDec (ID,tyfields,exp)(*#line 637.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 4, ( result, FUNCTION1left, exp1right), rest671)
end
|  ( 41, ( ( _, ( MlyValue.exp exp, _, exp1right)) :: _ :: ( _, ( MlyValue.ID ID2, _, _)) :: _ :: _ :: ( _, ( MlyValue.tyfields tyfields, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, FUNCTION1left, _)) :: rest671)) => let val  result = MlyValue.dec ((*#line 98.59 "tiger.grm"*)Ast.FunctypeDec (ID1,tyfields,ID2,exp)(*#line 641.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 4, ( result, FUNCTION1left, exp1right), rest671)
end
|  ( 42, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.tyfields tyfields, _, _)) :: ( _, ( _, LBRACE1left, _)) :: rest671)) => let val  result = MlyValue.ty ((*#line 101.40 "tiger.grm"*)Ast.record (tyfields)(*#line 645.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 5, ( result, LBRACE1left, RBRACE1right), rest671)
end
|  ( 43, ( ( _, ( MlyValue.ID ID, _, ID1right)) :: _ :: ( _, ( _, ARRAY1left, _)) :: rest671)) => let val  result = MlyValue.ty ((*#line 102.40 "tiger.grm"*)Ast.array (ID)(*#line 649.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 5, ( result, ARRAY1left, ID1right), rest671)
end
|  ( 44, ( rest671)) => let val  result = MlyValue.tyfields ((*#line 104.42 "tiger.grm"*)[](*#line 653.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 6, ( result, defaultPos, defaultPos), rest671)
end
|  ( 45, ( ( _, ( MlyValue.ID ID2, _, ID2right)) :: _ :: ( _, ( MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  result = MlyValue.tyfields ((*#line 105.40 "tiger.grm"*)[(ID1,ID2)](*#line 657.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 6, ( result, ID1left, ID2right), rest671)
end
|  ( 46, ( ( _, ( MlyValue.tyfields tyfields, _, tyfields1right)) :: _ :: ( _, ( MlyValue.ID ID2, _, _)) :: _ :: ( _, ( MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  result = MlyValue.tyfields ((*#line 106.43 "tiger.grm"*)(ID1,ID2) :: tyfields(*#line 661.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 6, ( result, ID1left, tyfields1right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a 
end
end
structure Tokens : Tiger_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(ParserData.MlyValue.VOID,p1,p2))
fun NIL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(ParserData.MlyValue.VOID,p1,p2))
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(ParserData.MlyValue.ID i,p1,p2))
fun INT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(ParserData.MlyValue.INT i,p1,p2))
fun STRING (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(ParserData.MlyValue.STRING i,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(ParserData.MlyValue.VOID,p1,p2))
fun DIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(ParserData.MlyValue.VOID,p1,p2))
fun MUL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(ParserData.MlyValue.VOID,p1,p2))
fun GT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(ParserData.MlyValue.VOID,p1,p2))
fun GE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(ParserData.MlyValue.VOID,p1,p2))
fun LT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(ParserData.MlyValue.VOID,p1,p2))
fun LE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(ParserData.MlyValue.VOID,p1,p2))
fun AND (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(ParserData.MlyValue.VOID,p1,p2))
fun OR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(ParserData.MlyValue.VOID,p1,p2))
fun NE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(ParserData.MlyValue.VOID,p1,p2))
fun ASSIGN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(ParserData.MlyValue.VOID,p1,p2))
fun ARRAY (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(ParserData.MlyValue.VOID,p1,p2))
fun WHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(ParserData.MlyValue.VOID,p1,p2))
fun FOR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(ParserData.MlyValue.VOID,p1,p2))
fun TO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(ParserData.MlyValue.VOID,p1,p2))
fun DO (p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(ParserData.MlyValue.VOID,p1,p2))
fun LBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(ParserData.MlyValue.VOID,p1,p2))
fun RBRACK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(ParserData.MlyValue.VOID,p1,p2))
fun LBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(ParserData.MlyValue.VOID,p1,p2))
fun RBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(ParserData.MlyValue.VOID,p1,p2))
fun SEMICOLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(ParserData.MlyValue.VOID,p1,p2))
fun COLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 34,(ParserData.MlyValue.VOID,p1,p2))
fun OF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 35,(ParserData.MlyValue.VOID,p1,p2))
fun DOT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 36,(ParserData.MlyValue.VOID,p1,p2))
fun TYPE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 37,(ParserData.MlyValue.VOID,p1,p2))
fun VAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 38,(ParserData.MlyValue.VOID,p1,p2))
fun LET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 39,(ParserData.MlyValue.VOID,p1,p2))
fun IN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 40,(ParserData.MlyValue.VOID,p1,p2))
fun END (p1,p2) = Token.TOKEN (ParserData.LrTable.T 41,(ParserData.MlyValue.VOID,p1,p2))
fun FUNCTION (p1,p2) = Token.TOKEN (ParserData.LrTable.T 42,(ParserData.MlyValue.VOID,p1,p2))
fun BREAK (p1,p2) = Token.TOKEN (ParserData.LrTable.T 43,(ParserData.MlyValue.VOID,p1,p2))
end
end
