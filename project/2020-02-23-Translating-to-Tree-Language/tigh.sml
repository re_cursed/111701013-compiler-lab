val lexer = Mlex.makeLexer (fn n => TextIO.inputN (TextIO.stdIn, n))

fun foo (Token.keywords (a,b,c)) = (Color.keywords_color()^a,b,c)
  | foo (Token.symbols  (a,b,c)) = (Color.symbols_color()^a,b,c)
  | foo (Token.rest     (a,b,c)) = (Color.rest_color()^a,b,c)
  | foo (Token.digits   (a,b,c)) = (Color.digits_color()^a,b,c)
  | foo (Token.comments (a,b,c)) = (Color.comments_color()^a,b,c)
  | foo (Token.quoted   (a,b,c)) = (Color.quoted_color()^a,b,c)
  | foo Token.eof                = ("EOF",0,0)

fun gettokens l = let
    val x = lexer()
    val y = (foo x)
    in 
    if y=("EOF",0,0) then l else gettokens (l@[y])
    end

val l = gettokens []

fun fst(a,_,_) = a
fun sec(_,a,_) = a
fun thd(_,_,a) = a

fun printlines 0 = ""
  | printlines n = let
                   val _ = print "\n"
                   in
                   printlines (n-1)
                   end
            
fun printspaces 0 = ""
  | printspaces n = let
                    val _ = print " "
                    in 
                    printspaces (n-1)
                    end


fun printcode [] = print "\n"
  | printcode (x::xs) = let
  val _ = printlines (sec x);
  val _ = printspaces (thd x);
  val _ = print (fst x);
  in 
    printcode xs 
  end

val _ = printcode l;
(* val _ = print "\027[31mdd"; *)
(* val _ = print "ss\n" *)
