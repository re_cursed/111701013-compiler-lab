signature TEMP =
sig
    type temp 
    type label
    val newlabel : unit -> label
    val newtemp  : unit -> temp
end

structure Temp : TEMP =
struct
    val label = ref 0
    val temp = ref 0
    type temp = string
    type label = string
    fun newlabel () = let
                        val l = !label
                        val _ = label := l+1
                      in
                        "L" ^ (Int.toString(l))
                      end
    fun newtemp () = let
                        val t = !temp
                        val _ = temp := t+1
                     in
                        "T" ^ (Int.toString(t))
                     end
end