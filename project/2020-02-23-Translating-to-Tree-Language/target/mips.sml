structure MIPS = 
struct

    datatype regs = zero | at | v0 | v1 | a0 | a1 | a2 | a3
                  | t0 | t1 | t2 | t3 | t4 | t5 | t6 | t7 
                  | s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7 
                  | t8 | t9 | k0 | k1 | gp | sp | fp | ra

    datatype aluop = ADD
                   | ADDU
                   | AND
                   | DIV
                   | DIVU
                   | MUL
                   | MULO    (* multiply without overflow *)
                   | MULOU   (* multiply with overflow *)
                   | SUB
                   | SUBU
                   | NOR
                   | OR
                   | XOR
                   | SLLV
                   | SRAV
                   | SRLV
                   | SLT
                   | SLTU
                   | SEQ
                   | SGE
                   | SGEU
                   | SGT
                   | SGTU
                   | SLE
                   | SLEU
                   | SNE

    datatype immop = ADDI
                   | ADDIU
                   | ANDI
                   | XORI
                   | ORI
                   | SLTI
                   | SLTIU

    datatype binop = ABS
                   | NEG
                   | NEGU
                   | NOT

    datatype isop  = SLL
                   | SRA
                   | SRL

    datatype fop   = FADD
                   | FSUB
                   | FMUL
                   | FDIV

    datatype fcomp = FEQ
                   | FLE
                   | FLT

    datatype zbranch = BGEZ
                     | BGTZ
                     | BLEZ
                     | BLTZ
                     | BEQZ
                     | BNEZ
    
    datatype bbranch = BEQ
                     | BNE
                     | BGE
                     | BGT
                     | BLE
                     | BLT
                     | BGEU
                     | BGTU
                     | BLEU
                     | BLTU

    datatype ea = IMM of int
                | REG of regs
                | OFFSET of regs*int
                | LAB of string

    datatype ('l,'t) inst = BOP of binop*'t*'t
                          | OP of aluop*'t*'t*'t
                          | IOP of immop*'t*'t*int
                          | ISOP of isop*'t*'t*int
                          | FOP of fop*'t*'t*'t
                          | FCOMP of fcomp*'t*'t
                          | MOVE of 't*'t
                          | FMOVE of 't*'t
                          | LI of 't*int
                          | LABEL of 'l
                          | LA of 't*'l
                          | LB of 't*'l
                          | L_D of 't*ea      (* load double *)
                          | L_S of 't*ea      (* load float *)
                          | LW of 't*ea
                          | SW of 't*ea
                          | S_S of 't*ea       (* store float *)
                          | S_D of 't*ea       (* store double *)
                          | B1 of zbranch*'t*'l
                          | B2 of bbranch*'t*'t*'l
                          | JAL of ea
                          | J of ea
                          | JR of ea
                          | RETURN
                          | SYSCALL
                          | NOP
                          | BREAK of int

    fun print_reg zero = "$zero"
      | print_reg at   = "$at"
      | print_reg v0   = "$v0"
      | print_reg v1   = "$v1"
      | print_reg a0   = "$a0"
      | print_reg a1   = "$a1"
      | print_reg a2   = "$a2"
      | print_reg a3   = "$a3"
      | print_reg t0   = "$t0"
      | print_reg t1   = "$t1"
      | print_reg t2   = "$t2"
      | print_reg t3   = "$t3"
      | print_reg t4   = "$t4"
      | print_reg t5   = "$t5"
      | print_reg t6   = "$t6"
      | print_reg t7   = "$t7"
      | print_reg t8   = "$t8"
      | print_reg t9   = "$t9"
      | print_reg s0   = "$s0"
      | print_reg s1   = "$s1"
      | print_reg s2   = "$s2"
      | print_reg s3   = "$s3"
      | print_reg s4   = "$s4"
      | print_reg s5   = "$s5"
      | print_reg s6   = "$s6"
      | print_reg s7   = "$s7"
      | print_reg k0   = "$k0"
      | print_reg k1   = "$k1"
      | print_reg gp   = "$gp"
      | print_reg sp   = "$sp"
      | print_reg fp   = "$fp"
      | print_reg ra   = "$ra"

    fun print_label l = print l

    fun print_aluop ADD   = "add"
      | print_aluop ADDU  = "addu"
      | print_aluop AND   = "and"
      | print_aluop DIV   = "div"
      | print_aluop DIVU  = "divu"
      | print_aluop MUL   = "mul"
      | print_aluop MULO  = "mulo"
      | print_aluop MULOU = "mulou"
      | print_aluop NOR    = "nor"
      | print_aluop OR     = "or"
      | print_aluop SLLV   = "sllv"
      | print_aluop SRAV   = "srav"
      | print_aluop SRLV   = "srlv"
      | print_aluop SUB    = "sub"
      | print_aluop SUBU   = "subu"
      | print_aluop XOR    = "xor"
      | print_aluop SLT    = "slt"
      | print_aluop SLTU   = "sltu"
      | print_aluop SEQ    = "seq"
      | print_aluop SGE    = "sge"
      | print_aluop SGEU   = "sgeu"
      | print_aluop SGT    = "sgt"
      | print_aluop SGTU   = "sgtu"
      | print_aluop SLE    = "sle"
      | print_aluop SLEU   = "sleu"
      | print_aluop SNE    = "sne"
      
    fun print_immop ADDI  = "addi"
      | print_immop ADDIU = "addiu"
      | print_immop ANDI  = "andi"
      | print_immop ORI   = "ori"
      | print_immop XORI  = "xori"
      | print_immop SLTI  = "slti"
      | print_immop SLTIU = "sltiu"

    fun print_binop ABS  = "abs"
      | print_binop NEG  = "neg"
      | print_binop NEGU = "negu"
      | print_binop NOT  = "not"

    fun print_isop SLL   = "sll"
      | print_isop SRA   = "sra"
      | print_isop SRL   = "srl"

    fun print_zb BGEZ    = "bgez"
      | print_zb BGTZ    = "bgtz"
      | print_zb BLEZ    = "blez"
      | print_zb BLTZ    = "bltz"
      | print_zb BEQZ    = "beqz"
      | print_zb BNEZ    = "bnez"

    fun print_bb BEQ    = "beq"
      | print_bb BNE    = "bne"
      | print_bb BGE    = "bge"
      | print_bb BGEU   = "bgeu"
      | print_bb BGT    = "bgt"
      | print_bb BGTU   = "bgtu"
      | print_bb BLE    = "ble"
      | print_bb BLEU   = "bleu"
      | print_bb BLT    = "blt"
      | print_bb BLTU   = "bltu"

    fun print_fop FADD = "add.s"
      | print_fop FSUB = "sub.s"
      | print_fop FMUL = "mul.s"
      | print_fop FDIV = "div.s"

    fun print_fcomp FEQ = "c.eq.s"
      | print_fcomp FLE = "c.le.s"
      | print_fcomp FLT = "c.lt.s"

    fun print_ea (IMM a)      = Int.toString a
      | print_ea (REG r)      = print_reg r
      | print_ea (OFFSET(r,n)) = (Int.toString n) ^ "(" ^ (print_reg r) ^ ")"
      | print_ea (LAB l)      = l

    fun print_space 0 = ""
      | print_space n = " " ^ (print_space (n-1))

    fun print_inst (BOP(b,r1,r2))      = (print_space 10) ^ (print_binop b) ^ (print_space (10 - (String.size (print_binop b)))) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (print_reg r2) ^ "\n"
      | print_inst (OP(alu,r1,r2,r3))  = (print_space 10) ^ (print_aluop alu) ^ (print_space (10 - (String.size (print_aluop alu)))) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (print_reg r2) ^ "," ^ (print_space (6 - (String.size (print_reg r2)))) ^ (print_reg r3) ^ "\n"
      | print_inst (IOP(imm,r1,r2,n))  = (print_space 10) ^ (print_immop imm) ^ (print_space (10 - (String.size (print_immop imm)))) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (print_reg r2) ^ "," ^ (print_space (6 - (String.size (print_reg r2)))) ^ (Int.toString n) ^ "\n"
      | print_inst (ISOP(imm,r1,r2,n)) = (print_space 10) ^ (print_isop imm) ^ (print_space (10 - (String.size (print_isop imm)))) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (print_reg r2) ^ "," ^ (print_space (6 - (String.size (print_reg r2)))) ^ (Int.toString n) ^ "\n"
      | print_inst (FOP(f,r1,r2,r3))   = (print_space 10) ^ (print_fop f) ^ (print_space (10 - (String.size (print_fop f)))) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (print_reg r2) ^ "," ^ (print_space (6 - (String.size (print_reg r2)))) ^ (print_reg r3) ^ "\n"
      | print_inst (FCOMP(fc,r1,r2))   = (print_space 10) ^ (print_fcomp fc) ^ (print_space (10 - (String.size (print_fcomp fc)))) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (print_reg r2) ^ "\n"
      | print_inst (MOVE(r1,r2))       = (print_space 10) ^ "move" ^ (print_space 6) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (print_reg r2) ^ "\n"
      | print_inst (FMOVE(r1,r2))      = (print_space 10) ^ "move.s" ^ (print_space 6) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (print_reg r2) ^ "\n"
      | print_inst (LI(r1,n))          = (print_space 10) ^ "li" ^ (print_space 8) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (Int.toString n) ^ "\n"
      | print_inst (LABEL(l))          = l ^ ":\n"
      | print_inst (LA(r,l))           = (print_space 10) ^ "la" ^ (print_space 8) ^ (print_reg r) ^ "," ^ (print_space (6 - (String.size (print_reg r)))) ^ l ^ "\n"
      | print_inst (LW(r,add))         = (print_space 10) ^ "lw" ^ (print_space 8) ^ (print_reg r) ^ "," ^ (print_space (6 - (String.size (print_reg r)))) ^ (print_ea add) ^ "\n"
      | print_inst (SW(r,add))         = (print_space 10) ^ "sw" ^ (print_space 8) ^ (print_reg r) ^ "," ^ (print_space (6 - (String.size (print_reg r)))) ^ (print_ea add) ^ "\n"
      | print_inst (B1(zb,r,l))        = (print_space 10) ^ (print_zb zb) ^ (print_space (10 - (String.size (print_zb zb)))) ^ (print_reg r) ^ "," ^ (print_space (6 - (String.size (print_reg r)))) ^ l ^ "\n"
      | print_inst (B2(bb,r1,r2,l))    = (print_space 10) ^ (print_bb bb) ^ (print_space (10 - (String.size (print_bb bb)))) ^ (print_reg r1) ^ "," ^ (print_space (6 - (String.size (print_reg r1)))) ^ (print_reg r2) ^ "," ^ (print_space (6 - (String.size (print_reg r2)))) ^ l ^ "\n"
      | print_inst (JAL(add))          = (print_space 10) ^ "jal" ^ (print_space 7) ^ (print_ea add) ^ "\n"
      | print_inst (J(add))            = (print_space 10) ^ "j" ^ (print_space 9) ^ (print_ea add) ^ "\n"
      | print_inst (JR(add))           = (print_space 10) ^ "jr" ^ (print_space 8) ^ (print_ea add) ^ "\n"
      | print_inst (RETURN)            = (print_space 10) ^ "return\n"
      | print_inst (SYSCALL)           = (print_space 10) ^ "syscall\n"
      | print_inst (NOP)               = (print_space 10) ^ "nop\n"

    fun pretty []      = print "\n"
      | pretty (x::xs) = let
                          val _ = print (print_inst x)
                         in
                          pretty xs
                         end
    (* Test Case *)

    val l = [
          LW(t0,OFFSET(gp,0)),
          B1(BLTZ,t0,"def"),
          IOP(SLTI,t1,t0,3),
          B2(BEQ,t1,zero,"def"),
          ISOP(SLL,t0,t0,2),
          OP(ADD,t2,t0,gp),
          LW(t2,OFFSET(t2,1064)),
          JR(REG(t2)),
          LABEL("is0"),
          SW(zero,OFFSET(gp,28)),
          J(LAB("done")),
          LABEL("is1"),
          LABEL("is2"),
          IOP(ADDI,t0,zero,1),
          SW(t0,OFFSET(gp,32)),
          J(LAB("done")),
          LABEL("def"),
          IOP(ADDI,t0,zero,1),
          SW(t0,OFFSET(gp,28)),
          J(LAB("done")),
          LABEL("done")
        ]
end

