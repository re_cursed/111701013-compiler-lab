structure Env = struct

    structure Map = RedBlackMapFn (Ast.IdKey)
    type environment = Temp.temp Map.map
    
    fun newEnv env ((var, temp) :: var_temp_ls) = newEnv (IdMap.insert (env, var, temp)) var_temp_ls
        | newEnv env [] = env
    
    val emptyEnv = IdMap.empty
    val findVar = IdMap.find

end