signature TEMP =
sig
    type temp 
    type label
    val newlabel : unit -> label
    val newtemp  : unit -> temp
end

structure Temp : TEMP =
struct
    val lab = ref 0
    val tem = ref 0
    type temp = string
    type label = string
    fun newlabel () = let
                        val l = !lab
                        val _ = lab := l+1
                      in
                        "L" ^ (Int.toString(l))
                      end
    fun newtemp () = let
                        val t = !tem
                        val _ = tem := t+1
                     in
                        "T" ^ (Int.toString(t))
                     end
end