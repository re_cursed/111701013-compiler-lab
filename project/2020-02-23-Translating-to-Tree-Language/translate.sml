structure Translate = struct

    exception error

    structure Tr = Tree
    structure Te = Temp
    structure A  = Ast

    datatype varmap = varmap of (Temp.label option) * Env.environment

    datatype exp = Ex of Tr.exp
        | Nx of Tree.stm
        | Cx of Temp.label * Temp.label -> Tree.stm

    fun seq [st] = st
        | seq (st :: st_list) = Tree.SEQ (st, seq st_list)
        | seq [] = raise error

    fun unEx (Ex e) = e
        | unEx (Nx s) = Tree.ESEQ (s, Tree.CONST 0)
        | unEx (Cx con) = let
                
                val te = Temp.newtemp ()
                val t = Temp.newlabel ()
                val f = Temp.newlabel ()

            in
                Tree.ESEQ (seq [
                    Tree.MOVE (Tree.TEMP te, Tree.CONST 1),
                    con (t, f),
                    Tree.LABEL f,
                    Tree.MOVE (Tree.TEMP te, Tree.CONST 0),
                    Tree.LABEL t
                ], Tree.TEMP te)
            end

    fun unNx (Ex e) = Tree.EXP e
        | unNx (Nx s) = s
        | unNx (Cx con) = raise error
    
    fun unCx (Ex e) = (fn (t, f) => Tree.CJUMP (Tree.NE, e, Tree.CONST 0, t, f))
        | unCx (Nx s) = raise error
        | unCx (Cx c) = c

    fun seperateLastEle [] = raise error
        | seperateLastEle [x] = ([], x)
        | seperateLastEle (x :: xs) = let
                val (ls, last_ele) = seperateLastEle xs
            in
                (x :: ls, last_ele)
            end
   
     fun translateExp varmap (Ast.IntExp x) = Ex (Tree.CONST x)
        | translateExp varmap (Ast.OpExp (e1, bin_op, e2)) = let
                val ex1 = unEx (translateExp varmap e1)
                val ex2 = unEx (translateExp varmap e2)
                val ex = case bin_op of
                    Ast.PLUS => Tree.BINOP (Tree.PLUS, ex1, ex2)
                    | Ast.MINUS => Tree.BINOP (Tree.MINUS, ex1, ex2)
                    | Ast.MUL => Tree.BINOP (Tree.MUL, ex1, ex2)
                    | Ast.DIV => Tree.BINOP (Tree.DIV, ex1, ex2)
                    | Ast.EQ => unEx (Cx (fn (t, f) => Tree.CJUMP (Tree.EQ, ex1, ex2, t, f)))
                    | Ast.NE => unEx (Cx (fn (t, f) => Tree.CJUMP (Tree.NE, ex1, ex2, t, f)))
                    | Ast.GT => unEx (Cx (fn (t, f) => Tree.CJUMP (Tree.GT, ex1, ex2, t, f)))
                    | Ast.LT => unEx (Cx (fn (t, f) => Tree.CJUMP (Tree.LT, ex1, ex2, t, f)))
                    | Ast.GE => unEx (Cx (fn (t, f) => Tree.CJUMP (Tree.GE, ex1, ex2, t, f)))
                    | Ast.LE => unEx (Cx (fn (t, f) => Tree.CJUMP (Tree.LE, ex1, ex2, t, f)))
                    | Ast.AND => Tree.BINOP (Tree.AND, ex1, ex2)
                    | Ast.OR => Tree.BINOP (Tree.OR, ex1, ex2)
            in
                Ex ex
            end
        | translateExp varmap (Ast.NegExp e) = Ex (Tree.BINOP (Tree.MINUS, Tree.CONST 0, unEx (translateExp varmap e)))
        | translateExp varmap (Ast.IfThenExp (e1,e2)) = let
                  val t = Te.newlabel()
                  val f = Te.newlabel()
                in
                  (Nx(seq[unCx (transExp varmap e1) (t,f),
                                        Tr.LABEL t,
                                        unNx (transExp varmap e2),
                                        Tr.LABEL f]),varmap)
                end
        | translateExp varmap (Ast.IfExp (e1, e2, e3)) = let
                  val r = Te.newtemp()
                  val t = Te.newlabel()
                  val f = Te.newlabel()
                  val join = Te.newlabel()
                in
                  (Ex (Tr.ESEQ(seq[unCx (transExp varmap e1) (t,f),
                                  Tr.LABEL t,
                                  Tr.MOVE(Tr.TEMP r,unEx (transExp varmap e2)),
                                  Tr.JUMP(Tr.NAME(join),[join]),
                                  Tr.LABEL f,
                                  Tr.MOVE(Tr.TEMP r, unEx (transExp varmap e2)),
                                  Tr.LABEL join],
                              Tr.TEMP r)),varmap)
                end
        | translateExp varmap (Ast.WhileExp (e1, e2)) = let
                val cnd = unCx (translateExp varmap e1)
                val cont = Temp.newlabel ()
                val t = Temp.newlabel ()
                val end = Temp.newlabel ()
                val (varmap (_, env)) = varmap
                val new_varmap = varmap (SOME end, env) 
                val stmts = seq [
                    Tree.LABEL cont,
                    cnd (t, end),
                    Tree.LABEL t,
                    Tree.EXP (unEx (translateExp new_varmap e2)),
                    Tree.JUMP (Tree.NAME cont, [cont]),
                    Tree.LABEL end
                ]
            in
                Nx stmts
            end
        | translateExp varmap (Ast.ForExp (var, start_exp, end_exp, body_exp)) = let
                val (varmap (_, env)) = varmap
                val loop_temp = Temp.newtemp ()
                val new_env = Env.newEnv env [(var, loop_temp)]
                
                val start_ex = unEx (translateExp varmap start_exp)
                val end_ex = unEx (translateExp varmap end_exp)

                val loop_label = Temp.newlabel ()
                val cont_label = Temp.newlabel ()
                val end_label = Temp.newlabel ()
                val eval_endex_temp = Temp.newtemp ()

                val new_varmap = varmap (SOME end_label, new_env)
                val body_stmt = unNx (translateExp new_varmap body_exp)
                
                val stmts = seq [
                    Tree.MOVE (Tree.TEMP eval_endex_temp, end_ex),
                    Tree.MOVE (Tree.TEMP loop_temp, start_ex),
                    Tree.LABEL loop_label,
                    Tree.CJUMP (Tree.NE, Tree.TEMP loop_temp, Tree.TEMP eval_endex_temp, cont_label, end_label),
                    Tree.LABEL cont_label,
                    body_stmt,
                    Tree.MOVE (Tree.TEMP loop_temp, Tree.BINOP (Tree.PLUS, Tree.TEMP loop_temp, Tree.CONST 1)),
                    Tree.JUMP (Tree.NAME loop_label, [loop_label]),
                    Tree.LABEL end_label
                ]
            in
                Nx stmts
            end
        | translateExp (varmap (break_label_opt, _)) Ast.Break = (
            case break_label_opt of
                NONE => raise error
                | SOME break_label => Nx (Tree.JUMP (Tree.NAME break_label, [break_label]))
        )
        | translateExp (varmap (_, env)) (Ast.Lval (Ast.Var var)) = let
                val var_temp_opt = Env.findVar (env, var)
            in
                case var_temp_opt of
                    NONE => raise error
                    | SOME var_temp => Ex (Tree.TEMP var_temp)
            end
        | translateExp varmap (Ast.Assignment (Ast.Var var, ex)) = let
                val e = unEx (translateExp varmap ex)
                val (varmap (_, env)) = varmap
                val var_temp_opt = Env.findVar (env, var)
                val var_temp = case var_temp_opt of
                    NONE => raise error
                    | SOME vt => vt
            in
                Nx (Tree.MOVE (Tree.TEMP var_temp, e))
            end
        (* Empty Let block would give empty sequence error *)
        | translateExp varmap (Ast.LetStmt (dec_list, exp_list)) = let 
                val (new_varmap, stmt_nx) = addDec varmap [] dec_list
                val stmt = unNx stmt_nx
                val (ls, last_ele) = seperateLastEle exp_list
                val new_stmt = seq (stmt :: map (unNx o translateExp new_varmap) ls)
            in
                Ex (Tree.ESEQ (new_stmt, unEx (translateExp new_varmap last_ele)))
            end
        | translateExp _ _ = raise error

    and translateDec varmap (Ast.Vardec (var, var_type_opt, ex)) = let
                val e = unEx (translateExp varmap ex)
                val (varmap (_, env)) = varmap
                val new_var_temp = Temp.newtemp ()
                val new_env = Env.newEnv env [(var, new_var_temp)]
            in
                (
                    new_env,
                    Nx (Tree.MOVE (Tree.TEMP new_var_temp, e))
                )
            end
        | translateDec _ _ = raise error

    and addDec prev_varmap prev_stmts (dec :: dec_ls) = let
                val (new_env, stmt_nx) = translateDec prev_varmap dec
                val (varmap (break_label_opt, _)) = prev_varmap
                val new_varmap = varmap (break_label_opt, new_env)
            in
                addDec new_varmap (prev_stmts @ [unNx stmt_nx]) dec_ls
            end
        | addDec final_varmap final_stmt_list [] = (final_varmap, Nx (seq final_stmt_list))

    fun translateProg (Ast.Expression exp) = unEx (translateExp (varmap (NONE, Env.emptyEnv)) exp)
        | translateProg (Ast.Decs dec_list) = (let
                val (_, stmt_list) = addDec (varmap (NONE, Env.emptyEnv)) [] dec_list
            in
                unEx stmt_list
            end)
end