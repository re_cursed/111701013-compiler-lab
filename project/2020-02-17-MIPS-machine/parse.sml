structure Parse =
struct
  
    structure TigerLrVals = TigerLrValsFun(structure Token = LrParser.Token)
    structure TigerLex    = TigerLexFun(structure Tokens = TigerLrVals.Tokens)
    structure TigerParser = Join(structure ParserData = TigerLrVals.ParserData
                                 structure Lex        = TigerLex
                                 structure LrParser   = LrParser
                                )

    val lexer = TigerParser.makeLexer(fn n => TextIO.inputN(TextIO.stdIn,n))

    fun print_error (s,i:int,_) = TextIO.output(TextIO.stdErr, "Error, line" ^ (Int.toString i) ^ "," ^ s ^ "\n")

    val (program,_) = TigerParser.parse(0,lexer,print_error,())
    
    val currIndent = ref 0
    fun inc_indent () = currIndent := !currIndent + 2
    fun dec_indent () = currIndent := !currIndent - 2

    fun space 0 = ""
      | space n = " " ^ (space (n-1))

    fun leave () = let val n = !currIndent in space n end

    fun iprint_Op Ast.PLUS  = "+"
      | iprint_Op Ast.MINUS = "-"
      | iprint_Op Ast.MUL   = "*"
      | iprint_Op Ast.DIV   = "/"
      | iprint_Op Ast.GT    = ">"
      | iprint_Op Ast.GE    = ">="
      | iprint_Op Ast.LT    = "<"
      | iprint_Op Ast.LE    = "<="
      | iprint_Op Ast.EQ    = "="
      | iprint_Op Ast.AND   = "&"
      | iprint_Op Ast.OR    = "|"
      | iprint_Op Ast.NE    = "<>"

    fun iprint_exp (Ast.NilExp)                 = "nil"
      | iprint_exp (Ast.IntExp s)               = s
      | iprint_exp (Ast.ID s)                   = s
      | iprint_exp (Ast.OpExp (e,oper,ee))      = (iprint_exp e) ^ " " ^ (iprint_Op oper) ^ " " ^ (iprint_exp ee)
      | iprint_exp (Ast.NegExp e)               = "-" ^ (iprint_exp e)
      | iprint_exp (Ast.AssignExp (s,e))        = s ^ ":=" ^ (iprint_exp e)
      | iprint_exp (Ast.IfExp (e,ee,eee))       = let 
                                                    val str1 = "if " ^ (iprint_exp e) ^ " then\n"
                                                    val _    = inc_indent()
                                                    val str2 = (leave()) ^ (iprint_exp ee) ^ "\n"
                                                    val _    = dec_indent()
                                                    val str3 = (leave()) ^ "else\n"
                                                    val _    = inc_indent()
                                                    val str4 = (leave()) ^ (iprint_exp eee)
                                                    val _    = dec_indent()
                                                in
                                                    str1^str2^str3^str4
                                                end
      | iprint_exp (Ast.IfThenExp (e,ee))       = let
                                                    val str1 = "if " ^ (iprint_exp e) ^ " then\n"
                                                    val _    = inc_indent()
                                                    val str2 = (leave()) ^ (iprint_exp ee) ^ "\n"
                                                    val _    = dec_indent()
                                                in 
                                                    str1^str2
                                                end
      | iprint_exp (Ast.WhileExp (e,ee))        = let
                                                    val str1 = "while " ^ (iprint_exp e) ^ " do\n"
                                                    val _    = inc_indent()
                                                    val str2 = (leave()) ^ (iprint_exp ee)
                                                    val _    = dec_indent
                                                in 
                                                    str1^str2
                                                end
      | iprint_exp(Ast.ForExp (s,e,ee,eee))     = let
                                                    val str1 = "for " ^ s ^ " := " ^ (iprint_exp e) ^ " to " ^ (iprint_exp ee) ^ "\n"
                                                    val _    = inc_indent()
                                                    val str2 = (leave()) ^ "do " ^ (iprint_exp eee)
                                                    val _    = dec_indent()
                                                in
                                                    str1^str2
                                                end
      | iprint_exp (Ast.ArrayExp (s,e,ee))      = s ^ " [ " ^ (iprint_exp e) ^ " ] of " ^ (iprint_exp ee)
      | iprint_exp (Ast.FunCall (s,ls))         = s ^ "(" ^ (iprint_funparams ls) ^ ")"
      | iprint_exp (Ast.BreakExp)               = "break"
      | iprint_exp (Ast.LetExp (dl,el))         = let
                                                    val str1 = "let\n"
                                                    val _     = inc_indent()
                                                    val str2 = iprint_decs dl
                                                    val _    = dec_indent()
                                                    val str3 = (leave()) ^ "in\n"
                                                    val _    = inc_indent()
                                                    val str4 = iprint_exps el
                                                    val _    = dec_indent()
                                                    val str5 = "\n" ^ (leave()) ^ "end"
                                                in
                                                    str1^str2^str3^str4^str5
                                                end
      | iprint_exp (Ast.LValue v)               = iprint_lvalue v

    and iprint_exps []      = ""
      | iprint_exps ([e])     = (leave()) ^ (iprint_exp e)
      | iprint_exps (e::es) = (leave()) ^ (iprint_exp e) ^ ";\n" ^ (iprint_exps es)

    and iprint_dec (Ast.TypeDec (s,typ))            = "type " ^ s ^ " = " ^ (iprint_typ typ)
      | iprint_dec (Ast.VarDec (s,e))               = "var " ^ s ^ " := " ^ (iprint_exp e)
      | iprint_dec (Ast.VartypeDec (s,ss,e))        = "var " ^ s ^ " : " ^ ss ^ " := " ^ (iprint_exp e)
      | iprint_dec (Ast.FuncDec (s,ssl,e))          = let
                                                        val str1 = "function " ^ s ^ " (" ^ (iprint_ssl ssl) ^ ") =\n"
                                                        val _    = inc_indent()
                                                        val str2 = (leave()) ^ (iprint_exp e)
                                                        val _    = dec_indent()
                                                    in 
                                                        str1^str2
                                                    end
      | iprint_dec (Ast.FunctypeDec (s,ssl,ss,e))    = let
                                                        val str1 = "function " ^ s ^ " (" ^ (iprint_ssl ssl) ^ ") : " ^ ss ^ " =\n"
                                                        val _    = inc_indent()
                                                        val str2 = (leave()) ^ (iprint_exp e)
                                                        val _    = dec_indent()
                                                    in 
                                                        str1^str2
                                                    end
                                                        
 

    and iprint_ssl []             = ""
      | iprint_ssl ([(s,ss)])     = s ^ " : " ^ ss
      | iprint_ssl ((s,ss) :: ls) = s ^ " : " ^ ss ^ ", " ^ (iprint_ssl ls)

    and iprint_decs []            = ""
      | iprint_decs (d::ds)       = (leave()) ^ (iprint_dec d) ^ "\n" ^ (iprint_decs ds)

    and iprint_funparams []      = ""
      | iprint_funparams ([e])   = (iprint_exp e)
      | iprint_funparams (e::el) = (iprint_exp e) ^ ", " ^ (iprint_funparams el)

    and iprint_typ (Ast.alias s)    = s
      | iprint_typ (Ast.record ssl) = iprint_ssl ssl
      | iprint_typ (Ast.array s)    = "array of " ^ s

    and iprint_lvalue (Ast.SubscriptVar (s,e)) = s ^ "[" ^ (iprint_exp e) ^ "]"
      | iprint_lvalue (Ast.SimpleVar (s))      = s
      | iprint_lvalue (Ast.FieldVar (l,s))     = s ^ (iprint_lvalue l)  

    fun pri (Ast.Expression e)   = iprint_exp e 
      | pri (Ast.Declaration ds) = iprint_decs ds

    val a = pri program 
    val _ = print a
    val _ = print "\n"


end