structure Token =
struct
  
    datatype token = keywords of string*int*int
                   | symbols  of string*int*int
                   | digits   of string*int*int
                   | rest     of string*int*int
                   | comments of string*int*int
                   | quoted   of string*int*int
                   | eof

end