type lineno              = int
type pos                 = lineno
val posRef               = ref 0
val lineRef              = ref 0

fun updateline n         = lineRef := !(lineRef) + n
fun updatepos  n         = posRef  := !(posRef) + n
fun resetpos   ()        = posRef  := 0

type svalue              = Tokens.svalue
type ('a,'b) token       = ('a,'b) Tokens.token
type lexresult           = (svalue,pos) token

fun eof ()               = Tokens.EOF (!lineRef,!posRef)

%%

%header (functor TigerLexFun(structure Tokens : Tiger_TOKENS));
whitespace = [\ \t\];
rest = [a-zA-Z];
digits = [0-9];

%%

\n               => (updateline 1; lex());
{whitespace}+    => (updatepos (String.size yytext); lex());

{digits}+        => (Tokens.INT (yytext, !lineRef, !posRef));


"+"              => (updatepos (String.size yytext); Tokens.PLUS        (!lineRef, !posRef));
"-"              => (updatepos (String.size yytext); Tokens.MINUS       (!lineRef, !posRef));
"*"              => (updatepos (String.size yytext); Tokens.MUL         (!lineRef, !posRef));
"/"              => (updatepos (String.size yytext); Tokens.DIV         (!lineRef, !posRef));
">"              => (updatepos (String.size yytext); Tokens.GT          (!lineRef, !posRef));
">="             => (updatepos (String.size yytext); Tokens.GE          (!lineRef, !posRef));
"<"              => (updatepos (String.size yytext); Tokens.LT          (!lineRef, !posRef));
"<="             => (updatepos (String.size yytext); Tokens.LE          (!lineRef, !posRef));
"="              => (updatepos (String.size yytext); Tokens.EQ          (!lineRef, !posRef));
"&"              => (updatepos (String.size yytext); Tokens.AND         (!lineRef, !posRef));
"<>"             => (updatepos (String.size yytext); Tokens.NE          (!lineRef, !posRef));
"|"              => (updatepos (String.size yytext); Tokens.OR          (!lineRef, !posRef));
":="             => (updatepos (String.size yytext); Tokens.ASSIGN      (!lineRef, !posRef));
"array"          => (updatepos (String.size yytext); Tokens.ARRAY       (!lineRef, !posRef));
"if"             => (updatepos (String.size yytext); Tokens.IF          (!lineRef, !posRef));
"then"           => (updatepos (String.size yytext); Tokens.THEN        (!lineRef, !posRef));
"else"           => (updatepos (String.size yytext); Tokens.ELSE        (!lineRef, !posRef));
"while"          => (updatepos (String.size yytext); Tokens.WHILE       (!lineRef, !posRef));
"for"            => (updatepos (String.size yytext); Tokens.FOR         (!lineRef, !posRef));
"to"             => (updatepos (String.size yytext); Tokens.TO          (!lineRef, !posRef));
"do"             => (updatepos (String.size yytext); Tokens.DO          (!lineRef, !posRef));
"of"             => (updatepos (String.size yytext); Tokens.OF          (!lineRef, !posRef));
"["              => (updatepos (String.size yytext); Tokens.LBRACK      (!lineRef, !posRef));
"]"              => (updatepos (String.size yytext); Tokens.RBRACK      (!lineRef, !posRef));
"{"              => (updatepos (String.size yytext); Tokens.LBRACE      (!lineRef, !posRef));
"}"              => (updatepos (String.size yytext); Tokens.LBRACK      (!lineRef, !posRef));
"("              => (updatepos (String.size yytext); Tokens.LPAREN      (!lineRef, !posRef));
")"              => (updatepos (String.size yytext); Tokens.RPAREN      (!lineRef, !posRef));
","              => (updatepos (String.size yytext); Tokens.COMMA       (!lineRef, !posRef));
";"              => (updatepos (String.size yytext); Tokens.SEMICOLON   (!lineRef, !posRef));
":"              => (updatepos (String.size yytext); Tokens.COLON       (!lineRef, !posRef));
"of"             => (updatepos (String.size yytext); Tokens.OF          (!lineRef, !posRef));
"."              => (updatepos (String.size yytext); Tokens.DOT         (!lineRef, !posRef));
"type"           => (updatepos (String.size yytext); Tokens.TYPE        (!lineRef, !posRef));
"var"            => (updatepos (String.size yytext); Tokens.VAR         (!lineRef, !posRef));
"let"            => (updatepos (String.size yytext); Tokens.LET         (!lineRef, !posRef));
"in"             => (updatepos (String.size yytext); Tokens.IN          (!lineRef, !posRef));
"end"            => (updatepos (String.size yytext); Tokens.END         (!lineRef, !posRef));
"function"       => (updatepos (String.size yytext); Tokens.FUNCTION    (!lineRef, !posRef));
"break"          => (updatepos (String.size yytext); Tokens.BREAK       (!lineRef, !posRef));


{rest}+          => (updatepos (String.size yytext); Tokens.ID (yytext, !lineRef, !posRef));